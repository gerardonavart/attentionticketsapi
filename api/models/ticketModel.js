const mongoose = require('mongoose')
const uniqueValidator = require('mongoose-unique-validator')
const { baseMessageBuilder } = require('../middleware/httpResponseHandler')

let Schema = mongoose.Schema

let TicketSchema = new Schema({

    customerId: {
        type: Schema.Types.ObjectId,
        required: [true, baseMessageBuilder(409, 'AUTHNOK002')],
        ref: 'as_customers'
    },
    branchId: {
        type: String,
        required: [true, baseMessageBuilder(409, 'AUTHNOK002')]
    },
    branchName: {
        type: String,
        required: [true, baseMessageBuilder(409, 'AUTHNOK002')]
    },
    branchTime: {
        type: String,
        required: [true, baseMessageBuilder(409, 'AUTHNOK002')]
    },
    activityType: {
        type: String,
        enum: { values: ['Ejecutivo', 'Ventanilla', 'Banquero']},
    },
    isReusable: {
        type: Boolean,
        default: false
    },
    isCustomer: {
        type: Boolean,
        default: true
    },
    creationDate: {
        type: Date
    }
})


TicketSchema.plugin( uniqueValidator, baseMessageBuilder(409, 'AUTHNOK003') )

module.exports = mongoose.model('at_tickets', TicketSchema)