const mongoose = require('mongoose')
const uniqueValidator = require('mongoose-unique-validator')
const { baseMessageBuilder } = require('../middleware/httpResponseHandler')

let Schema = mongoose.Schema

let CustomerSchema = new Schema({
    name: {
        type: String,
        required: [true],
        trim: true
    },
    lastName: {
        type: String,
        required: [true],
        trim: true
    },
    secondLastName: {
        type: String,
        required: [true],
        trim: true
    },
    gender: {
        type: String,
        enum: { values: ['male', 'female', 'not_specified']}
    },
    phone: {
        type: String,
        required: false,
        trim: true
    },
    img: {
        type: String,
        required: false
    },
    email: {
        type: String,
        required: [true],
        trim: true,
        unique: true 
    },
    password: {
        type: String,
        required: [true],
        trim: true
    },
    userRole: {
        type: String,
        default: 'CUSTOMER_ROLE',
        enum: { values: ['ADMIN_ROLE', 'CUSTOMER_ROLE', 'PUBLIC_ROLE']}
    },
    isGoogleAuth: {
        type: Boolean,
        default: false
    },
    userStatus: {
        type: Boolean,
        default: true
    },
    lastLoginDate: {
        type: Date,
        required: [true]
    },
    createdDate: {
        type: Date,
        required: [true]
    },
    lastUpdateDate: {
        type: Date
    }
})

CustomerSchema.methods.toJSON = function () {
    let customerObject = this.toObject()
    delete customerObject.password
    return customerObject
}

CustomerSchema.plugin( uniqueValidator, baseMessageBuilder(409, 'AUTHNOK003') )

module.exports = mongoose.model('at_customers', CustomerSchema)