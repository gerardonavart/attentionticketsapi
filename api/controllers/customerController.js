const customerModel = require('../models/customerModel')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const { validationResult } = require('express-validator')
const _ = require('underscore')
const saltRounds = 10;
//const { responseMessageBuilder, responseObjectMessageBuilder } = require('../middleware/httpResponseHandler.js')
let errorResult;

module.exports = {
    login: function (req, res) {
        errorResult = validationResult(req)

        if (!errorResult.isEmpty()) return res.status(404).json({ code: 'AUTHNOK001', message: 'Error, autenticación inválida' })

        customerModel.findOne({
            email: req.body.email
        }, (errorResult, customerFound) => {

            if (errorResult) return res.status(500).json({ code: 'AUTHNOK007', message: 'Error interno del servidor' })

            if (!customerFound || !bcrypt.compareSync(req.body.password, customerFound.password)) {
                 return res.status(404).json({ code: 'AUTHNOK001', message: 'Error, autenticación inválida' })
                }
            
            let authToken = jwt.sign({
                user: {
                    uId: customerFound._id,
                    uEmail: customerFound.email,
                    uRole: customerFound.userRole,
                    uStatus: customerFound.status
                }
            }, process.env.TOKEN_SEED, {
                expiresIn: process.env.TOKEN_LIFETIME
            })

            res.setHeader("auth_token", authToken)
            res.setHeader('Access-Control-Expose-Headers', 'auth_token')
            return res.status(200).json( {customerId: customerFound._id} )
            //return responseObjectMessageBuilder(res, 200, null, {uId: customerFound._id, authToken: authToken})
        })

    },
    create: function (req, res) {
        errorResult = validationResult(req)
        if (!errorResult.isEmpty()) return res.status(400).json({ code: 'AUTHNOK005', message: 'Error, autenticación asociada' }, errorResult.array())

        customerModel.create({
            name: req.body.name,
            lastName: req.body.lastName,
            secondLastName: req.body.secondLastName,
            gender: req.body.gender,
            email: req.body.email,
            password: bcrypt.hashSync(req.body.password, saltRounds),
            userRole: req.body.role,
            createdDate: new Date(),
            lastLoginDate: new Date()
        }, (customerError) => {
            return customerError ? res.status(400).json({ code: 'AUTHNOK004', message: 'Error, alta incorrecta', data: {customerError} }) : res.status(201).json({ code: 'AUTHOK002', message: 'OK, alta efectuada' })
        })

    },
    update: function (req, res) {
        let customerId = req.params.customerId
        let body = _.pick(req.body, ['email', 'role', 'name', 'lastName', 'secondLastName'])
        customerModel.findByIdAndUpdate(customerId, body, {
            new: true,
            runValidators: true,
            context: 'query'
        }, (err, accountUpdated) => {
            if (err || !accountUpdated) {
                return !accountUpdated ? res.status(400).json({ code: 'AUTHNOK008', message: 'Error en actualización' }) : res.status(404).json({ code: 'AUTHNOK009', message: 'Error, cuenta inexistente' })
            }
            return res.status(200).json({ code: 'AUTHOK003', message: 'OK, actualización efectuada' })
        })
    },
    deactivate: function (req, res) {
        let customerId = req.params.customerId
        customerModel.findByIdAndUpdate(customerId, {
            userStatus: false
        }, (err, accountDeactivated) => {

            if (err) return res.status(400).json({ code: 'AUTHNOK010', message: 'Error, acción desactivar incorrecta' })
            if (!accountDeactivated) return res.status(404).json({ code: 'AUTHNOK009', message: 'Error, cuenta inexistente' })
            return res.status(200).json({ code: 'AUTHOK005', message: 'OK, acción desactivar efectuada' })

        })
    },
    delete: function (req, res) {
        let customerId = req.params.customerId
        customerModel.findByIdAndRemove(customerId, (err, accountDeleted) => {
            if (err) return res.status(400).json({ code: 'AUTHNOK011', message: 'Error, eliminación incorrecta' })
            if (!accountDeleted) return res.status(404).json({ code: 'AUTHNOK009', message: 'Error, cuenta inexistente' })
            return res.status(200).json({ code: 'AUTHOK004', message: 'OK, eliminación efectuada' })
        })
    },
    listAll: function (req, res) {
        let from = Number(req.query.from)
        let to = Number(req.query.to)

        customerModel.find({
                userStatus: true
            })
            .skip(from)
            .limit(to)
            .exec((err, accountsListed) => {
                if (err) return res.status(400).json({ code: 'AUTHNOK002', message: 'Error, parámetros faltantes o incorrectos' })

                customerModel.count({
                    status: true
                }, (err, countedUsers) => {
                    return res.status(200).json({ code: 'AUTHOK001', message: 'OK, operación efectuada', data: { users: accountsListed, count: countedUsers } })
                })
            })
    },
    listOne: function (req, res) {
        customerModel.findOne({
            _id: req.params.customerId
        }, (errorResult, customerFound) => {
            if (errorResult) return res.status(400).json({ code: 'AUTHNOK002', message: 'Error, parámetros faltantes o incorrectos' })
            if (!customerFound) return res.status(404).json({ code: 'AUTHNOK001', message: 'Error, autenticación inválida' })
            return res.status(200).json({ code: 'AUTHOK001', message: 'OK, operación efectuada', data: {customerFound} })
        })
    }
}