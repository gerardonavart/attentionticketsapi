const ticketModel = require('../models/ticketModel')
const { validationResult } = require('express-validator')
const _ = require('underscore')
//const { responseMessageBuilder, responseObjectMessageBuilder } = require('../middleware/httpResponseHandler.js')
let errorResult;

module.exports = {
    listOne: function (req, res) {
        ticketModel.findOne({
            _id: req.params.ticketId
        }, (errorResult, ticket) => {
            if (errorResult) return res.status(400).json({ code: 'AUTHNOK002', message: 'Error, parámetros faltantes o incorrectos' })
            if (!ticket) return res.status(404).json({ code: 'AUTHNOK001', message: 'Error, autenticación inválida' })
            return res.status(200).json({ code: 'AUTHOK001', message: 'OK, operación efectuada', data: { ticket } })
        })
    },
    listAll: function (req, res) {
        ticketModel.find({
            customerId: req.params.customerId
        }).limit(5).exec((errorResult, ticketsFound) => {
            if (errorResult) return res.status(400).json({ code: 'AUTHNOK002', message: 'Error, parámetros faltantes o incorrectos' })
            if (!ticketsFound) return res.status(404).json({ code: 'AUTHNOK001', message: 'Error, autenticación inválida' })
            return res.status(200).json({ code: 'AUTHOK001', message: 'OK, operación efectuada', data: { ticketsFound } })
        })
    },
    create: function (req, res) {
        errorResult = validationResult(req)
        if (!errorResult.isEmpty()) return res.status(400).json({ code: 'AUTHNOK005', message: 'Error, ticket asociado' }, errorResult.array())

        ticketModel.create({
            customerId: req.body.customerId,
            branchId: req.body.branchId,
            branchName: req.body.branchName,
            branchTime: req.body.branchTime,
            activityType: req.body.activityType,
            creationDate: new Date()
        }, (ticketError) => {
            return ticketError ? res.status(400).json({ code: 'AUTHNOK004', message: 'Error, alta incorrecta', data: ticketError }) : res.status(201).json({ code: 'AUTHOK002', message: 'OK, alta efectuada' })
        })

    },
    update: function (req, res) {
        let ticketId = req.params.ticketId
        let body = _.pick(req.body, ['branchId', 'branchName', 'branchLatitude', 'branchLongitude', 'branchTime', 'activityType'])
        ticketModel.findByIdAndUpdate(ticketId, body, {
            new: true,
            runValidators: true,
            context: 'query'
        }, (err, ticketUpdated) => {
            if (err || !ticketUpdated) {
                return !ticketUpdated ? res.status(400).json({ code: 'AUTHNOK008', message: 'Error en actualización' }) : res.status(404).json({ code: 'AUTHNOK009', message: 'Error, elemento inexistente' })
            }
            return res.status(200).json({ code: 'AUTHOK003', message: 'OK, actualización efectuada' })
        })
    },
    delete: function (req, res) {
        let ticketId = req.params.ticketId
        ticketModel.findByIdAndRemove(ticketId, (err, ticketDeleted) => {
            if (err) return res.status(400).json({ code: 'AUTHNOK011', message: 'Error, eliminación incorrecta' })
            if (!ticketDeleted) return res.status(404).json({ code: 'AUTHNOK009', message: 'Error, elemento inexistente' })
            return res.status(200).json({ code: 'AUTHOK004', message: 'OK, eliminación efectuada' })
        })
    }
}