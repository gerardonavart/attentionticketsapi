const errorMessages = new Map([
    ['AUTHNOK001', 'Error, autenticación inválida'],
    ['AUTHNOK002', 'Error, parámetros faltantes o incorrectos'],
    ['AUTHNOK003', 'Error, parámetros no pueden ser iguales'],
    ['AUTHNOK004', 'Error, alta incorrecta'],
    ['AUTHNOK005', 'Error, autenticación asociada'],
    ['AUTHNOK006', 'Error, eliminación de autenticación fallida'],
    ['AUTHNOK007', 'Error interno del servidor'],
    ['AUTHNOK008', 'Error en actualización'],
    ['AUTHNOK009', 'Error, cuenta inexistente'],
    ['AUTHNOK010', 'Error, acción desactivar incorrecta'],
    ['AUTHNOK011', 'Error, eliminación incorrecta'],
    ['AUTHNOK012', 'Error, auth-token caducado'],
    ['AUTHNOK013', 'Error, usuario con nivel insuficiente']
]);

const successMessages = new Map([
    ['AUTHOK001', 'OK, operación efectuada'],
    ['AUTHOK002', 'OK, alta efectuada'],
    ['AUTHOK003', 'OK, actualización efectuada'],
    ['AUTHOK004', 'OK, eliminación efectuada'],
    ['AUTHOK005', 'OK, acción desactivar efectuada']
])

let responseMessage = (messageType, operationCode) => {
    return messageType ? successMessages.get(operationCode) : errorMessages.get(operationCode)
}

module.exports = { responseMessage }