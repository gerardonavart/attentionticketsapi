const httpStatus = new Map([
    [200, 'OK'],
    [201, 'Created'],
    [400, 'Bad request'],
    [401, 'Unauthorized'],
    [403, 'Forbidden'],
    [404, 'Not found'],
    [409, 'Functional error'],
    [500, 'Internal server error'] 
]);

let httpStatusManager = (httpStatusCode) => {
    return httpStatus.get(httpStatusCode)
}

module.exports = { httpStatusManager }