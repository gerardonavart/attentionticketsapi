const { httpStatusManager } = require('../helpers/httpStatusHelper')
const { responseMessage } = require('../helpers/responseMessageHelper')

let baseMessageBuilder = (httpStatusCode, operationStatusCode) => {
    return {
        result: httpStatusManager(httpStatusCode),
        code: operationStatusCode,
        message: responseMessage(getMessageType(httpStatusCode), operationStatusCode)
    }
}

let baseObjectMessageBuilder = (httpStatusCode, operationStatusCode, operationMessage) => {
    return {
        result: httpStatusManager(httpStatusCode),
        code: operationStatusCode,
        message: operationMessage
    }
}

let responseMessageBuilder = (response, httpStatusCode, operationStatusCode) => {
    response.status(httpStatusCode).json({
        result: httpStatusManager(httpStatusCode),
        code: operationStatusCode,
        message: responseMessage(getMessageType(httpStatusCode), operationStatusCode)
    })
}

let responseObjectMessageBuilder = (response, httpStatusCode, operationStatusCode, operationMessage) => {
    response.status(httpStatusCode).json({
        result: httpStatusManager(httpStatusCode),
        code: operationStatusCode,
        message: operationMessage
    })
}

function getMessageType(httpStatusCode) {
    const successHttpStatus = [200, 201]
    return successHttpStatus.includes(httpStatusCode)
}

module.exports = { responseMessageBuilder, baseMessageBuilder, responseObjectMessageBuilder, baseObjectMessageBuilder }