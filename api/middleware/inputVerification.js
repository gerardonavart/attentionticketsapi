const { check } = require('express-validator')

let loginVerification = [ check('email').isEmail(), check('password').isLength({ min: 8 }) ] 

module.exports = { loginVerification }