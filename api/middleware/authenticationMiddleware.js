const jwt = require('jsonwebtoken')
const { responseMessageBuilder } = require('../middleware/httpResponseHandler.js')

let tokenVerification = (req, res, next) => {
    let token = req.get('auth_token')
    jwt.verify(token, process.env.TOKEN_SEED, (err, decoded) => {
        if (err) return responseMessageBuilder(res, 401, 'AUTHNOK012')
        req.user = decoded.user
        next()
    })
}
let adminRoleVerification = (req, res, next) => {
    let user = req.user
    if (user.uRole !== 'ADMIN_ROLE') return responseMessageBuilder(res, 401, 'AUTHNOK013')
    next()
}
let customerRoleVerification = (req, res, next) => {
    let user = req.user
    if (user.uRole !== 'CUSTOMER_ROLE') return responseMessageBuilder(res, 401, 'AUTHNOK013')
    next()
}

let fullAccessVerification = (req, res, next) => {
    let user = req.user
    if (user.uRole !== 'CUSTOMER_ROLE' || user.uRole !== 'ADMIN_ROLE') return responseMessageBuilder(res, 401, 'AUTHNOK013')
    next()
}

module.exports = { tokenVerification, adminRoleVerification, customerRoleVerification, fullAccessVerification }