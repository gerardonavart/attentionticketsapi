const express = require('express')
const router = express.Router()
const { loginVerification } = require('../middleware/inputVerification')
const { tokenVerification, customerRoleVerification } = require('../middleware/authenticationMiddleware')

const customerController = require('../controllers/customerController')

router.post('/customer/login', [loginVerification], customerController.login)
router.post('/customer/create', [loginVerification], customerController.create)
router.get('/customer', [tokenVerification, customerRoleVerification], customerController.listAll)
router.get('/customer/:customerId', [tokenVerification, customerRoleVerification], customerController.listOne)
//router.post('/customer/google', customerController.google)
router.patch('/customer/:customerId', [tokenVerification, customerRoleVerification], customerController.update)
router.delete('/customer/deactivate/:customerId', [tokenVerification, customerRoleVerification], customerController.deactivate)
router.delete('/customer/delete/:customerId', [tokenVerification, customerRoleVerification], customerController.delete)


module.exports = router