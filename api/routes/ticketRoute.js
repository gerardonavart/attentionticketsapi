const express = require('express')
const router = express.Router()
const { tokenVerification, customerRoleVerification } = require('../middleware/authenticationMiddleware')

const ticketController = require('../controllers/ticketController')

router.post('/ticket/create', [tokenVerification, customerRoleVerification], ticketController.create)
router.get('/ticket/customer/:customerId', [tokenVerification, customerRoleVerification], ticketController.listAll)
router.get('/ticket/:ticketId', [tokenVerification, customerRoleVerification], ticketController.listOne)
router.patch('/ticket/:ticketId', [tokenVerification, customerRoleVerification], ticketController.update)
router.delete('/ticket/:ticketId', [tokenVerification, customerRoleVerification], ticketController.delete)


module.exports = router