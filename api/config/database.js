const mongoose = require('mongoose')

const mongoDBConn = 'mongodb://localhost:27017/attentiontickets_db'

mongoose.connect(mongoDBConn, { useNewUrlParser: true, useUnifiedTopology: true }, (err) => {
    if (err) throw err
    console.log('Connected to database')
})

module.exports = mongoose