process.env.PORT = process.env.PORT || 3000

process.env.NODE_ENV = process.env.NODE_ENV || 'dev'

process.env.TOKEN_LIFETIME = '10m'

process.env.TOKEN_SEED = process.env.TOKEN_SEED || 'attentionTicketsDS'

//process.env.CLIENT_ID = process.env.CLIENT_ID || '411759010776-pt8ge2q5e1jjcshrnh3soin8rlaf46t4.apps.googleusercontent.com'

let dbURL;

if (process.env.NODE_ENV === 'dev') {
    dbURL = 'mongodb://localhost:27017/attentiontickets_db';
} else {
    dbURL = process.env.MONGO_URI;
}
process.env.dbURL = dbURL;