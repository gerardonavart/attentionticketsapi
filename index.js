require('./api/config/config');

const express = require('express')
const morgan = require('morgan')
const bodyParser = require('body-parser')
const app = express()

const mongoose = require('./api/config/database')

app.use(morgan('dev'))
app.use(express.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

mongoose.connection.on('error', console.error.bind(console, 'Database connection error'))

app.use('/customers/v0', require('./api/routes/customerRoute'))
app.use('/tickets/v0', require('./api/routes/ticketRoute'))

app.listen(process.env.PORT, () => console.log('Listening on port ' + process.env.PORT + '\nEnvironment: ' + process.env.NODE_ENV))